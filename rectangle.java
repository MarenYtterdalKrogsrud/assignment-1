import java.util.Scanner; //import java scanner

class rectangle{
    public static void main(String [] args){  

        Scanner scan = new Scanner(System.in); //declare the scanner object and pass the input stream in the constructor
        int width; //declare integer variables since we are going to work with numbers
        int height; 
        boolean loopActive = true; // variable to keep loop active
       
        while(loopActive == true) { // loop while boolean is true

            do { // prompt user with rectangle width
                System.out.println("Enter the width of the rectangle with a number:");
                    while(!scan.hasNextInt()){ // validates the input to check if it is a valid number
                        System.out.println("That is not a number! Try again");
                        scan.next();
                    }
                width = scan.nextInt();  // saves the number from the input into width variable.
            } while (width <= 0); // the condition 

                
            do { // prompt for height
                System.out.println("Enter the height of the rectangle with a number:");
                    while(!scan.hasNextInt()){
                        System.out.println("That is not a number! Try again");
                        scan.next();
                    }
                    height = scan.nextInt();  
            } while (height <= 0);
            

            draw_rectangle(width,height); // calling the draw_rectangle method  

            // asks the user if they want to continue
            System.out.print("Do you want to continue y/n?"); 
            while(!scan.hasNext("[yn]")){ // validates the input to check if the user wrote y or n. Continues until correct input
                System.out.println("Press y for yes and n for no");
                scan.next();
            }  
            String yn = scan.next();
            if(yn.equals("n")) {
                loopActive = false; // ends loop
                return;
            }
        }
    }


   

    static void draw_rectangle(int w, int h){ // draw rectangle method
        for(int i = 1; i <= h; i++){ // loop for height

            for (int u = 1; u <= w; u++){ // creates the width of the rectangle 

                if (i == 1 || i == h) // if at top or bottom of rectangle
                    System.out.print("#");

                else if (u == 1 || u == w) // if start or end of line
                        System.out.print("#");

                else // if none of the above create space
                    System.out.print(" ");
            }
            
            System.out.println(""); // new line
        }
    }             
}
